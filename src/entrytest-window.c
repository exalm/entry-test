#include "entrytest-config.h"
#include "entrytest-window.h"

struct _EntrytestWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
};

G_DEFINE_TYPE (EntrytestWindow, entrytest_window, GTK_TYPE_APPLICATION_WINDOW)

static void
entrytest_window_class_init (EntrytestWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Entrytest/entrytest-window.ui");
  gtk_widget_class_bind_template_child (widget_class, EntrytestWindow, header_bar);
}

static void
entrytest_window_init (EntrytestWindow *self)
{
  GdkScreen *screen;
  GtkCssProvider *provider;

  gtk_widget_init_template (GTK_WIDGET (self));

  screen = gdk_screen_get_default ();
  provider = gtk_css_provider_new ();
  gtk_css_provider_load_from_resource (provider, "/org/gnome/Entrytest/custom.css");
  gtk_style_context_add_provider_for_screen (screen, provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}
