#include "entrytest-config.h"
#include "entrytest-entry.h"

struct _EntrytestEntry
{
  GtkBox parent_instance;
};

G_DEFINE_TYPE (EntrytestEntry, entrytest_entry, GTK_TYPE_BOX)

static void
entrytest_entry_on_focus_in (GtkWidget *widget,
                             GdkEvent  *event,
                             gpointer   user_data)
{
  EntrytestEntry *self = ENTRYTEST_ENTRY (user_data);
  GtkStyleContext *context;
  GtkStateFlags flags;

  context = gtk_widget_get_style_context (GTK_WIDGET (self));
  flags = gtk_style_context_get_state (context);
  flags |= GTK_STATE_FLAG_FOCUSED;
  gtk_style_context_set_state (context, flags);
}

static void
entrytest_entry_on_focus_out (GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   user_data)
{
  EntrytestEntry *self = ENTRYTEST_ENTRY (user_data);
  GtkStyleContext *context;
  GtkStateFlags flags;

  context = gtk_widget_get_style_context (GTK_WIDGET (self));
  flags = gtk_style_context_get_state (context);
  flags &= ~GTK_STATE_FLAG_FOCUSED;
  gtk_style_context_set_state (context, flags);
}

static void
entrytest_entry_class_init (EntrytestEntryClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Entrytest/entrytest-entry.ui");
  gtk_widget_class_bind_template_callback (widget_class, entrytest_entry_on_focus_in);
  gtk_widget_class_bind_template_callback (widget_class, entrytest_entry_on_focus_out);

  gtk_widget_class_set_css_name (widget_class, "entry");
}

static void
entrytest_entry_init (EntrytestEntry *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
