#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define ENTRYTEST_TYPE_ENTRY (entrytest_entry_get_type())

G_DECLARE_FINAL_TYPE (EntrytestEntry, entrytest_entry, ENTRYTEST, ENTRY, GtkBox)

G_END_DECLS
